//
//  NetworkErrorsModel.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 01/03/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation
struct NetworkResponseError: Codable {
    let statusCode: Int
    let statusMessage: String
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}
