//
//  Authintication.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 28/06/2018.
//  Copyright © 2018 Nasser AlHamad. All rights reserved.
//

import Foundation
struct CreateNewToken: Codable, Equatable {
    let success: Bool
    let expiresAt: Date
    let requestToken: String
    
    enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
    
    public static func == (lhs: CreateNewToken, rhs: CreateNewToken) -> Bool{
        if (lhs.expiresAt == rhs.expiresAt){
            if (lhs.success == rhs.success){
                if (lhs.requestToken == rhs.requestToken){
                    return true
                }
            }
        }
        return false
    }
    
}
struct LoginWithToken: Codable {
    let success: Bool
    let expiresAt, requestToken: String
    
    
    enum CodingKeys: String, CodingKey {
        case success
        case expiresAt = "expires_at"
        case requestToken = "request_token"
    }
}
struct Session: Decodable {
    let success: Bool
    let sessionID: String
    
    enum CodingKeys: String, CodingKey {
        case success
        case sessionID = "session_id"
    }
}
