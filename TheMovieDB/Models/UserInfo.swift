//
//  User.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 29/06/2018.
//  Copyright © 2018 Nasser AlHamad. All rights reserved.
//

import Foundation
public struct UserInfo{
    var Token = ""
    var sessionID = ""
    static var shared = UserInfo()
    
    private init(){
        
    }
}
