//
//  popularMovies.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 27/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation

struct PopularMovies: Codable {
    let results: [Result]
    init() {
        results = [Result]()
    }
}

struct Result: Codable {
    let voteAverage: Double
    let title, posterPath: String
    
    init() {
        voteAverage = 0
        title = ""
        posterPath = ""
    }
    
    enum CodingKeys: String, CodingKey {
        case voteAverage = "vote_average"
        case title
        case posterPath = "poster_path"
    }
}
