//
//  popularMovieTableViewCell.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 27/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import UIKit

class popularMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var movieRate: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
