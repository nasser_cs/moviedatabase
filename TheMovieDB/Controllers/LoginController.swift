//
//  LoginController.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 28/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation


protocol LoginSituation {
    func LoginSuccess()
    func LoginFails(errorMessage: String)
}



public class LoginController {
    var delegate: LoginSituation?
    
    var savedState = 0
    
    func login(userName: String, password: String){
        
        if (userNameValidation(userName: userName) && passwordNameValidation(password: password)){
        createNewToken { (createNewTokenResponseStatus) in
            switch createNewTokenResponseStatus {
            case .success(let newToken):
                
                UserInfo.shared.Token = newToken.requestToken
                self.saveExpirationDate(expiresAt: newToken.expiresAt)
                
                self.loginWithToken(userName, password, completion: { (loginWithTokenResponseStatus) in
                    switch loginWithTokenResponseStatus {
                    case .success(let resonse):
                        self.createNewSession(completion: { (createNewSessionResponseStatus) in
                            switch createNewSessionResponseStatus{
                            case .success(let sessionDetail):
                                self.saveSessionID(sessionID: sessionDetail.sessionID)
                                DispatchQueue.main.sync {
                                    self.delegate?.LoginSuccess()
                                }
                            case .faild(let error):
                                DispatchQueue.main.sync {
                                    self.delegate?.LoginFails(errorMessage: error.localizedDescription)
                                }
                            }
                        })
                    case .faild(let error):
                        DispatchQueue.main.sync {
                        self.delegate?.LoginFails(errorMessage: error.localizedDescription)
                        }
                    }
                })
                
            case .faild(let error):
                DispatchQueue.main.sync {
                    self.delegate?.LoginFails(errorMessage: error.localizedDescription)
                }
            }
        }
        }else{
            self.delegate?.LoginFails(errorMessage: "Please validate inputs")
        }
    }
    
    func createNewToken(completion: @escaping (networkResponseStatus<CreateNewToken>) -> Void){
        LoginServices.createNewTokenCall { (networkCallResult) in
            switch networkCallResult {
            case .success(let tokenDetail):
                completion(.success(tokenDetail))
            case .faild(let error):
                print (error)
            }
        }
    }
    
    func loginWithToken(_ userName: String, _ password: String, completion: @escaping (networkResponseStatus<LoginWithToken>) -> Void){
        LoginServices.loginWithTokenCall(token: UserInfo.shared.Token, userName: userName, password: password) { (loginWithTokenCallResult) in
            switch loginWithTokenCallResult{
            case .success(let loginDetails):
                completion(.success(loginDetails))
            case .faild(let error):
                completion(.faild(error))
            }
        }
    }
    
    func createNewSession(completion: @escaping (networkResponseStatus<Session>)-> Void){
        LoginServices.createSessionCall(token: UserInfo.shared.Token, completion: { (sessionResult) in
            switch sessionResult{
            case .success(let sessionDetail):
                if (sessionDetail.success){
                    completion(.success(sessionDetail))
                }else{
                    completion(.faild(NetworkErrorCases.SessionNotAccepted))
                }
            case .faild(let error):
                completion(.faild(error))
            }
        })
    }
    
    

    func saveExpirationDate(expiresAt: Date) -> Bool{
        UserDefaults.standard.set(expiresAt, forKey: "tokenExpirationDate")
        return true
    }
    
    func saveSessionID(sessionID: String) -> Bool{
        UserDefaults.standard.set(sessionID, forKey: "sessionID")
        return true
    }
    
    
    func userNameValidation(userName: String?) -> Bool{
        if let userName = userName, !userName.isEmpty {
            let range = NSRange(location: 0, length: userName.utf16.count)
            let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$")
            
            if (regex.matches(in: userName, options: [], range: range).count > 0) {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    func passwordNameValidation(password: String?) -> Bool{
        if let password = password, !password.isEmpty {
            return true
        }else{
            return false
        }
    }
    
    

}
