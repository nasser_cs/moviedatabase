//
//  ApplicationEnteranceController.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 02/03/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation
import UIKit

protocol userLoginStatus {
    func userLogedIn()
    func userNeedsLogin()
}


public class ApplicationEnteranceController{
    var delegate: userLoginStatus?
    
    
    func checkLogin(){
        if let expirationDate =  getPersistentExpirationDate() {
            if (isTokenValid(expirationDate: expirationDate)){
                if (fetchSessionID()){
                    delegate?.userLogedIn()
                }
                else{
                 delegate?.userNeedsLogin()
                }
            }else{
                delegate?.userNeedsLogin()
            }
        }else{
            delegate?.userNeedsLogin()
        }
    }
    func getPersistentExpirationDate() -> Date?{
        return UserDefaults.standard.value(forKey: "tokenExpirationDate") as? Date
    }
    func isTokenValid(expirationDate: Date) -> Bool {
        return expirationDate > Date()
    }
    func fetchSessionID() -> Bool{
        if let sessionId = UserDefaults.standard.value(forKey: "sessionID") as? String{
            UserInfo.shared.sessionID = sessionId
            return true
        }else{
            return false
        }
    }
    
}
