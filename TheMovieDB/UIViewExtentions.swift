//
//  UIViewExtentions.swift
//  OnTheMapProject
//
//  Created by Nasser AlHamad on 22/07/2018.
//  Copyright © 2018 Nasser AlHamad. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    func hideKeyboardOnTappingAnyWhare() {
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit, completion: @escaping (Bool) -> Void) {
        contentMode = mode
        

        let request = URLRequest(url: url, cachePolicy: URLRequest.CachePolicy.returnCacheDataElseLoad, timeoutInterval: 10)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let response = response else {DispatchQueue.main.async() {completion(false)}; return}
            
            if let httpURLResponse = response as? HTTPURLResponse{
                if (200 ... 299 ~= httpURLResponse.statusCode){
                    if let data = data {
                        let image = UIImage(data: data)
                        DispatchQueue.main.async() {
                            completion(true)
                            self.image = image
                        }
                    }else {completion(false)}
                }else {completion(false)}
            }else {completion(false)}
            }.resume()
        }

    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit, completion: @escaping (Bool)->Void) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode, completion: completion)
    }
}
