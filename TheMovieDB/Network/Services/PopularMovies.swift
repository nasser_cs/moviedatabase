//
//  popularMovies.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 28/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation

public class MoviesServices {
    static func getPopularMoviesCall(completion: @escaping (networkResponseStatus<PopularMovies>) -> Void){
        print("Request \n \(MoviesURLs.getURLForPopularMovies())")
        let createTokenTask = URLSession.shared.dataTask(with: MoviesURLs.getURLForPopularMovies()) { (data, response, error) in
            let result =  getPopularMovies(data: data, response: response, error: error)
            completion(result)
        }
        createTokenTask.resume()
    }
    
    static func getPopularMovies(data: Data?, response: URLResponse?, error: Error?) -> networkResponseStatus<PopularMovies>{
        if let error = error {
            return .faild(NetworkErrorCases.error(error))
        }
        guard let data = data else {
            return .faild(NetworkErrorCases.dataNotFound)
        }
        do {
            print("Data in response")
            print(String(data: data, encoding: .utf8)!)
            let decoder = JSONDecoder()
            
            let response = try decoder.decode(PopularMovies.self, from: data)
            return .success(response)
        }catch let error{
            return .faild(NetworkErrorCases.error(error))
        }
    }
}
