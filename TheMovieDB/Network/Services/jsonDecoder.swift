//
//  jsonDecoder.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 28/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation

enum ErrorDecoding: Error{
    case ParsingFaild(String)
}


public class jsonDecoder {
    func decode<T: Codable> (data: Data, dateFormating: String?) throws -> T {
        let decoder = JSONDecoder()
        if let dateFormating = dateFormating{
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormating
            decoder.dateDecodingStrategy = .formatted(formatter)
        }
        do {
            let result = try decoder.decode(T.self, from: data)
            return result
        }catch{
            throw ErrorDecoding.ParsingFaild(error.localizedDescription)
        }
    }
}
