//
//  NetworkServices.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 25/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation



public class LoginServices {
    static func createNewTokenCall(completion: @escaping (networkResponseStatus<CreateNewToken>) -> Void){
        print("Request \n \(LoginURLs.getURLForCreateNewToken())")

        let createTokenTask = URLSession.shared.dataTask(with: LoginURLs.getURLForCreateNewToken()) { (data, response, error) in
            let result =  createToken(data: data, response: response, error: error)
            completion(result)
        }
        createTokenTask.resume()
    }
    
    
    static func createToken(data: Data?, response: URLResponse?, error: Error?) -> networkResponseStatus<CreateNewToken> {
        if let error = error {
            return .faild(.error(error))
        }
        guard let data = data else {
            return .faild(NetworkErrorCases.dataNotFound)
        }
        
        if let httpResponse = response as? HTTPURLResponse {
            print("Data in response \n")
            print(String(data: data, encoding: .utf8)!)
            if 200 ... 299 ~= httpResponse.statusCode {
                do {
                    let decoder = JSONDecoder()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss 'UTC'"
                    formatter.timeZone = TimeZone(secondsFromGMT: 0)
                    decoder.dateDecodingStrategy = .formatted(formatter)
                    let response = try decoder.decode(CreateNewToken.self, from: data)
                    return .success(response)
                }catch {
                    return .faild(NetworkErrorCases.parsingError)
                }
            }
            else if 400 ... 499 ~= httpResponse.statusCode {
                let decoder = JSONDecoder()
                let response = try? decoder.decode(NetworkResponseError.self, from: data)
                return .faild(NetworkErrorCases.responseError(responseError: response?.statusMessage ?? "Something went wrong!"))
            } else {
                return .faild(NetworkErrorCases.generalError)
            }
            
        }else{
            return .faild(NetworkErrorCases.responseError(responseError: "Something went wrong!"))
        }
    }
    
    // MARK: Login with token
    static func loginWithTokenCall(token: String, userName: String, password: String, completion: @escaping (networkResponseStatus<LoginWithToken>) -> Void){
        print("Request \n \(LoginURLs.loginWithTokenURL(token: token, userName: userName, password: password ))")

        let loginWithTokenDataTask = URLSession.shared.dataTask(with: LoginURLs.loginWithTokenURL(token: token, userName: userName, password: password )) { (data, response, error) in
            completion(loginWithToken(data: data, response: response, error: error))
        }
        loginWithTokenDataTask.resume()
    }
    
    static func loginWithToken(data: Data?, response: URLResponse?, error: Error?) -> networkResponseStatus<LoginWithToken>{
        if let error = error {
            return .faild(.error(error))
        }
        guard let data = data else {
            return .faild(NetworkErrorCases.dataNotFound)
        }
        if let httpResponse = response as? HTTPURLResponse {
            print("Data in response")
            print(String(data: data, encoding: .utf8)!)
            if 200 ... 299 ~= httpResponse.statusCode {
                do {
                    let decoder = JSONDecoder()
                    let parsedResponse = try decoder.decode(LoginWithToken.self, from: data)
                    return (.success(parsedResponse))
                }catch {
                    return .faild(NetworkErrorCases.parsingError)
                }
            }
            else if 400 ... 499 ~= httpResponse.statusCode {
                let decoder = JSONDecoder()
                let response = try? decoder.decode(NetworkResponseError.self, from: data)
                return .faild(NetworkErrorCases.responseError(responseError: response?.statusMessage ?? "Something went wrong!"))
            } else {
                return .faild(NetworkErrorCases.generalError)
            }
        }else{
            return .faild(NetworkErrorCases.responseError(responseError: "Something went wrong!"))
        }
    }
    
    
    static func createSessionCall(token: String, completion: @escaping (networkResponseStatus<Session>) -> Void ){
        print("Request \n \(LoginURLs.createSessionID(token: token))")

        let createSessionID_DataTask = URLSession.shared.dataTask(with: LoginURLs.createSessionID(token: token)) { (data, response, error) in
            completion(createSession(data: data,response: response,error: error))
        }
        createSessionID_DataTask.resume()
    }
    
    
    static func createSession (data: Data?, response: URLResponse?, error: Error?) -> networkResponseStatus<Session> {
        if let error = error {
            return .faild(.error(error))
        }
        guard let data = data else {
            return .faild(NetworkErrorCases.dataNotFound)
        }
        print("Data in response")
        print(String(data: data, encoding: .utf8)!)
        if let httpResponse = response as? HTTPURLResponse {
            if 200 ... 299 ~= httpResponse.statusCode {
                do {
                    let decoder = JSONDecoder()
                    let parsedResponse = try decoder.decode(Session.self, from: data)
                    return (.success(parsedResponse))
                }catch {
                    return .faild(NetworkErrorCases.parsingError)
                }
            }
            else if 400 ... 499 ~= httpResponse.statusCode {
                let decoder = JSONDecoder()
                let response = try? decoder.decode(NetworkResponseError.self, from: data)
                return .faild(NetworkErrorCases.responseError(responseError: response?.statusMessage ?? "Something went wrong!"))
            } else {
                return .faild(NetworkErrorCases.generalError)
            }
        }else{
            return .faild(NetworkErrorCases.responseError(responseError: "Something went wrong!"))
        }
    }
}
