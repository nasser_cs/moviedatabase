//
//  sharedInNetwork.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 03/03/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation
enum NetworkErrorCases {
    var localizedDescription: String {
        switch self {
        case .dataNotFound:
            return "Data Not Found!"
        case .error(let error):
            return error.localizedDescription
        case .generalError:
            return "Sorry an error occuerd"
        case.parsingError:
            return "Parsing failed"
            
        case .responseError(responseError: let error):
            return error
            
        case .SessionNotAccepted:
            return "Session Not accepted"
        }
    }
    
    case dataNotFound
    case SessionNotAccepted
    case responseError(responseError: String)
    case error(_: Error)
    case parsingError
    case generalError
}


enum networkResponseStatus<T: Decodable> {
    case success(T), faild(NetworkErrorCases)
}
