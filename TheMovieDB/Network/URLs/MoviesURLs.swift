//
//  MoviesURLs.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 28/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation
public class MoviesURLs{
    
    static func getURLForPopularMovies() -> URL {
        //https://api.themoviedb.org/3/movie/popular?api_key=df7a0c1e3437a9ce12948e06866a5f61
        var components = URLComponents()
        components.scheme = NetworkConstants.TMDB.ApiScheme_Https
        components.host = NetworkConstants.TMDB.ApiHost
        components.path = NetworkConstants.TMDB.ApiPath + NetworkConstants.URLextentions.Movies + NetworkConstants.URLextentions.PopularMovies
        
        components.queryItems = [URLQueryItem]()
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.ApiKey, value: NetworkConstants.TMDBParameterValues.ApiKey))
        
        
        return components.url!
    }
}
