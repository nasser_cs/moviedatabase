//
//  NetworkBase.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 28/06/2018.
//  Copyright © 2018 Nasser AlHamad. All rights reserved.
//

import Foundation
public class LoginURLs{
    
    static func getURLForCreateNewToken() -> URL {
        var components = URLComponents()
        components.scheme = NetworkConstants.TMDB.ApiScheme_Http
        components.host = NetworkConstants.TMDB.ApiHost
        components.path = NetworkConstants.TMDB.ApiPath + NetworkConstants.URLextentions.CreateNewToken
        components.queryItems = [URLQueryItem]()
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.ApiKey, value: NetworkConstants.TMDBParameterValues.ApiKey))
        
        
        return components.url!
    }
    
    
    static func loginWithTokenURL(token: String, userName: String, password: String) -> URL{
        var components = URLComponents()
        components.scheme = NetworkConstants.TMDB.ApiScheme_Http
        components.host = NetworkConstants.TMDB.ApiHost
        components.path = NetworkConstants.TMDB.ApiPath + NetworkConstants.URLextentions.ValidateWithLogin
        components.queryItems = [URLQueryItem]()
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.ApiKey, value: NetworkConstants.TMDBParameterValues.ApiKey))
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.RequestToken, value: token))
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.Username, value: userName))
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.Password, value: password))
        
        
        return components.url!
    }
    
    
    static func createSessionID(token: String) -> URL{
        var components = URLComponents()
        components.scheme = NetworkConstants.TMDB.ApiScheme_Http
        components.host = NetworkConstants.TMDB.ApiHost
        components.path = NetworkConstants.TMDB.ApiPath + NetworkConstants.URLextentions.CreateSessionID
        components.queryItems = [URLQueryItem]()
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.ApiKey, value: NetworkConstants.TMDBParameterValues.ApiKey))
        components.queryItems?.append(URLQueryItem(name: NetworkConstants.TMDBParameterKeys.RequestToken, value: token))
        
        
        return components.url!
    }
    
}
