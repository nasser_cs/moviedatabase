//
//  Constants.swift
//  MyFavoriteMovies
//
//  Created by Jarrod Parkes on 11/5/15.
//  Copyright © 2015 Udacity. All rights reserved.
//

import UIKit

// MARK: - Constants

struct NetworkConstants {
    
    // MARK: TMDB
    struct TMDB {
        static let ApiScheme_Http = "http"
        static let ApiScheme_Https = "https"
        static let ApiHost = "api.themoviedb.org"
        static let ApiPath = "/3"
    }
    
    // MARK: TMDB Parameter Keys
    struct TMDBParameterKeys {
        static let ApiKey = "api_key"
        static let RequestToken = "request_token"
        static let SessionID = "session_id"
        static let Username = "username"
        static let Password = "password"
    }
    
    // MARK: TMDB Parameter Values
    struct TMDBParameterValues {
        static let ApiKey = "df7a0c1e3437a9ce12948e06866a5f61"
    }
    
    // MARK: TMDB Response Keys
    struct TMDBResponseKeys {
        static let Title = "title"
        static let ID = "id"
        static let PosterPath = "poster_path"
        static let StatusCode = "status_code"
        static let StatusMessage = "status_message"
        static let SessionID = "session_id"
        static let RequestToken = "request_token"
        static let Success = "success"
        static let UserID = "id"
        static let Results = "results"
    }
    struct URLextentions {
        static let CreateNewToken = "/authentication/token/new"
        static let ValidateWithLogin = "/authentication/token/validate_with_login"
        static let CreateSessionID = "/authentication/session/new"
        static let Movies = "/movie"
        static let PopularMovies = "/popular"
    }
    struct PosterPrefex {
        static let posterBase = "://image.tmdb.org/t/p/w500"
        static let posterPrefex = TMDB.ApiScheme_Https + posterBase
    }
    

    
}
