//
//  NetworkServices.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 25/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case dataNotFound
}

enum networkResponseStatus<T: Decodable> {
    case success(T), faild(Error)
}

public class NetworkServices {
    static func createNewTokenCall(completion: @escaping (networkResponseStatus<CreateNewToken>) -> Void){
        let createTokenTask = URLSession.shared.dataTask(with: NetworkRequests.getURLForCreateNewToken()) { (data, response, error) in
            let result =  createToken(data: data, response: response, error: error)
            completion(result)
        }
        createTokenTask.resume()
    }
    
    
    
    static func createToken(data: Data?, response: URLResponse?, error: Error?) -> networkResponseStatus<CreateNewToken> {
        if let error = error {
            return .faild(error)
        }
        guard let data = data else {
            return .faild(NetworkError.dataNotFound)
        }
        do {
            debugPrint(String(data: data, encoding: .utf8)!)
            let decoder = JSONDecoder()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss 'UTC'"
            decoder.dateDecodingStrategy = .formatted(formatter)
            let response = try decoder.decode(CreateNewToken.self, from: data)
            return .success(response)
        }catch let error{
            return .faild(error)
        }
    }
    
    // MARK: Login with token
    static func loginWithTokenCall(token: String, userName: String, password: String, completion: @escaping (networkResponseStatus<LoginWithToken>) -> Void){
        let loginWithTokenDataTask = URLSession.shared.dataTask(with: NetworkRequests.loginWithTokenURL(token: token, userName: userName, password: password )) { (data, response, error) in
            completion(loginWithToken(data: data, response: response, error: error))
        }
        loginWithTokenDataTask.resume()
    }
    
    static func loginWithToken(data: Data?, response: URLResponse?, error: Error?) -> networkResponseStatus<LoginWithToken>{
        if let error = error {
            return (.faild(error))
        }
        guard let data = data else {
            return .faild(NetworkError.dataNotFound)
        }
        do {
            debugPrint(String(data: data, encoding: .utf8))
            let decoder = JSONDecoder()
            let parsedResponse = try decoder.decode(LoginWithToken.self, from: data)
            return (.success(parsedResponse))
        }
        catch let error{
            return (.faild(error))
        }
    }
    
    
    static func createSessionCall(token: String, completion: @escaping (networkResponseStatus<Session>) -> Void ){
        let createSessionID_DataTask = URLSession.shared.dataTask(with: NetworkRequests.createSessionID(token: token)) { (data, response, error) in
            completion(createSession(data: data,response: response,error: error))
        }
        createSessionID_DataTask.resume()
    }
    
    
    static func createSession (data: Data?, response: URLResponse?, error: Error?) -> networkResponseStatus<Session> {
        guard let data = data else {
            return .faild(NetworkError.dataNotFound)
        }
        do {
            debugPrint(String(data: data, encoding: .utf8))
            let decoder = JSONDecoder()
            let parsedResponse = try decoder.decode(Session.self, from: data)
            return (.success(parsedResponse))
            
        }catch let error{
            return (.faild(error))
        }
    }
}
