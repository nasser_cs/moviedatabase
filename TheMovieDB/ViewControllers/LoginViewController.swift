//
//  ViewController.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 27/06/2018.
//  Copyright © 2018 Nasser AlHamad. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, LoginSituation {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    var loginController = LoginController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginController.delegate = self
    }
    
    
    
    func LoginSuccess() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MoviesTableView")
        self.present(controller, animated: true, completion: nil)
        }
    
    func LoginFails(errorMessage: String) {
        let alert = UIAlertController(title: "Login Fails", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func Login(_ sender: Any) {
            self.loginController.login(userName: userName.text!, password: password.text!)
    }
}

