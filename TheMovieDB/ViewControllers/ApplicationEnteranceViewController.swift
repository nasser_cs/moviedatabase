//
//  ApplicationEnteranceViewController.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 02/03/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import UIKit

class ApplicationEnteranceViewController: UIViewController, userLoginStatus {

    var applicationEnteranceController = ApplicationEnteranceController()
    
    override func viewDidAppear(_ animated: Bool) {
        applicationEnteranceController.checkLogin()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        applicationEnteranceController.delegate = self
        
    }
    
    
    func userLogedIn(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MoviesTableView")
        self.present(controller, animated: true, completion: nil)
    }
    
    func userNeedsLogin(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginPage")
        self.present(controller, animated: true, completion: nil)
    }
}
