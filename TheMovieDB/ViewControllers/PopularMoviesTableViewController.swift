//
//  PopularMoviesTableViewController.swift
//  TheMovieDB
//
//  Created by Nasser AlHamad on 27/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import UIKit

class PopularMoviesTableViewController: UITableViewController {

    
    var popularMovies = PopularMovies()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var mockup = "{\"results\":[{\"vote_average\":6.8,\"title\":\"Alita: Battle Angel\",\"poster_path\":\"/xRWht48C2V8XNfzvPehyClOvDni.jpg\"}]}".data(using: .utf8)

        
        
        MoviesServices.getPopularMoviesCall { (response) in
            switch response{
            case .success(let fetchedMovies):
                self.popularMovies = fetchedMovies
                DispatchQueue.main.sync {
                    self.popularMovies = fetchedMovies
                    self.tableView.reloadData()
                }
            case .faild(let error):
                print(error)
            }
        }

    }
}

extension PopularMoviesTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return popularMovies.results.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! popularMovieTableViewCell
        cell.prepareForReuse()
        cell.movieTitle.text = popularMovies.results[indexPath.row].title
        cell.movieRate.text = String(popularMovies.results[indexPath.row].voteAverage)
        
        cell.movieImage.downloadedFrom(link: NetworkConstants.PosterPrefex.posterPrefex + self.popularMovies.results[indexPath.row].posterPath) { (successfullyDownloadedIcon) in
           
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(154)
    }
    
}
