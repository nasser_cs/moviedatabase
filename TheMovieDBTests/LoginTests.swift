//
//  LoginTests.swift
//  TheMovieDBTests
//
//  Created by Nasser AlHamad on 02/03/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import XCTest
@testable import TheMovieDB

class LoginTests: XCTestCase {

    var loginController = LoginController()
    
    func testUsernameValidation(){
        let invalidUsername = "_Nasser"
        XCTAssertFalse(loginController.userNameValidation(userName: invalidUsername))
        
        let validUserName = "Nasser"
        XCTAssertTrue(loginController.userNameValidation(userName: validUserName))
    }
    
    func testPasswordValidation(){
        let invalidPassword = ""
        XCTAssertFalse(loginController.passwordNameValidation(password: invalidPassword))
        
        let validPassword = "dsf3234a"
        XCTAssertTrue(loginController.userNameValidation(userName: validPassword))
    }   
    
    func testActualCreateNewTokenRequest(){
        print(loginController.savedState)
        let expectation = XCTestExpectation(description: "Creating token")
        LoginServices.createNewTokenCall { (response) in
            switch response{
            case .success(let response):
                XCTAssertNotNil(response.requestToken)
                expectation.fulfill()
            case .faild(let error):
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testParsingNewToken(){
        let dumyData = "{\"success\":true,\"expires_at\":\"2019-02-26 22:00:46 UTC\",\"request_token\":\"aa9f69ca4ada3a4a8284f9d7196335b9d64cb402\"}".data(using: .utf8)
        let decoder = JSONDecoder()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss 'UTC'"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        decoder.dateDecodingStrategy = .formatted(formatter)
        let decodedObject = try? decoder.decode(CreateNewToken.self, from: dumyData!)
        
        var dumyResponse = HTTPURLResponse(url: URL(string: "http://")!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let functionOutput = LoginServices.createToken(data: dumyData, response: dumyResponse, error: nil)
        
        switch functionOutput {
        case .success(let outputTobeCombared):
            XCTAssertEqual(decodedObject!, outputTobeCombared)
        default:
            XCTFail()
        }
    }
    func testGettingServerMessage() {
        let dummyData = "{\"status_code\":33,\"status_message\":\"Invalid request token: The request token is either expired or invalid.\"}".data(using: .utf8)
        
        let decoder = JSONDecoder()
        let decodedObject = try? decoder.decode(NetworkResponseError.self, from: dummyData!)
        
        let dumyResponse = HTTPURLResponse(url: URL(string: "http://")!, statusCode: 401, httpVersion: nil, headerFields: nil)
        let functionOutput = LoginServices.loginWithToken(data: dummyData!, response: dumyResponse, error: nil)
        
        switch functionOutput {
        case .faild(let responseCase):
            XCTAssertEqual(responseCase.localizedDescription, decodedObject?.statusMessage)
        default:
            XCTFail()
        }
    }
}
