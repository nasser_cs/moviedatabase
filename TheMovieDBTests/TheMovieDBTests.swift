//
//  TheMovieDBTests.swift
//  TheMovieDBTests
//
//  Created by Nasser AlHamad on 25/02/2019.
//  Copyright © 2019 Nasser AlHamad. All rights reserved.
//

import XCTest
@testable import TheMovieDB

class TheMovieDBTests: XCTestCase {

    var loginController = LoginController ()
    
    func testFetchingNotExistTokenExpirationDate(){
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        let ExpirationDate = ApplicationEnteranceController().getPersistentExpirationDate()
        XCTAssertNil(ExpirationDate)
    }
    
    func testTokenIsNotValid(){
        let passedDate = Calendar.current.date(byAdding: .year, value: -1, to: Date())
        let expirationValidite = ApplicationEnteranceController().isTokenValid(expirationDate: passedDate!)
        XCTAssertFalse(expirationValidite)
    }
    func testTokenIsValid(){
        let advancedDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        XCTAssertTrue(ApplicationEnteranceController().isTokenValid(expirationDate: advancedDate!))
    }
    func testFetchingSessionID(){
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        XCTAssertFalse(ApplicationEnteranceController().fetchSessionID())
        
        LoginController().saveSessionID(sessionID: "session123")
        XCTAssertTrue(ApplicationEnteranceController().fetchSessionID())
        
        if let sessionId = UserDefaults.standard.value(forKey: "sessionID") as? String{
            XCTAssertEqual(sessionId, "session123")
        }else{
            XCTFail()
            
        }
    }
}
